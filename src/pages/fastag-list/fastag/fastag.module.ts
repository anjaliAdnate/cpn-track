import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FastagPage } from './fastag';

@NgModule({
  declarations: [
    FastagPage,
  ],
  imports: [
    IonicPageModule.forChild(FastagPage),
  ],
})
export class FastagPageModule {}
