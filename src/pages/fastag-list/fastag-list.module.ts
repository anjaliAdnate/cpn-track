import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FastagListPage } from './fastag-list';

@NgModule({
  declarations: [
    FastagListPage,
  ],
  imports: [
    IonicPageModule.forChild(FastagListPage),
  ],
})
export class FastagListPageModule {}
