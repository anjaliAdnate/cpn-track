import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotifMapPage } from './notif-map';

@NgModule({
  declarations: [
    NotifMapPage,
  ],
  imports: [
    IonicPageModule.forChild(NotifMapPage),
  ],
})
export class NotifMapPageModule {}
