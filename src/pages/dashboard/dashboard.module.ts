import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardPage } from './dashboard';
import { TranslateModule } from '@ngx-translate/core';
// import { ContentDrawerComponent } from '../../components/content-drawer/content-drawer';
// import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DashboardPage,
    // ContentDrawerComponent
  ],
  imports: [
    IonicPageModule.forChild(DashboardPage),
    TranslateModule.forChild()
    // ComponentsModule
  ],
  exports: [
    // ContentDrawerComponent
  ]
})
export class DashboardPageModule {}
