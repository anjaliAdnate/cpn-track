import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ToastController } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';
import { AppLanguages } from '../../../providers/app-languages';
import { Language } from '../../../providers/app.model';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {
  isChecked: boolean;
  notif: string;
  isBooleanforLive: boolean;
  isBooleanforDash: boolean;
  mapKey: any;
  fuelKey: string;
  lankey: string;
  islogin: any;
  fuels: any[] = [];
  maps: string[] = [];
  selectedMapKey: string;
  lankeyname: string;
  languages: Language[];
  nightMode: boolean;

  constructor(
    public events: Events,
    private tts: TextToSpeech,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private apiCall: ApiServiceProvider,
    public translate: TranslateService,
    private appLang: AppLanguages) {
    this.fuels = ['LITRE', 'PERCENTAGE'];
    this.maps = [this.translate.instant('Normal'), this.translate.instant('Terrain'), this.translate.instant('Satellite')];
    this.languages = [...this.appLang.Languages];

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));

    if (this.islogin.fuel_unit === 'LITRE') {
      this.fuelKey = 'LITRE';
    } else {
      this.fuelKey = 'PERCENTAGE';
    }
    // this.fuelKey = this.translate.instant(this.islogin.fuel_unit);

    if (localStorage.getItem('MAP_KEY') != null) {
      this.selectedMapKey = localStorage.getItem('MAP_KEY');
    }
    if (localStorage.getItem("notifValue") != null) {
      this.notif = localStorage.getItem("notifValue");
      if (this.notif == 'true') {
        this.isChecked = true;
      } else {
        this.isChecked = false;
      }
    }

    if (localStorage.getItem('NightMode') != null) {
      if (localStorage.getItem('NightMode') === 'ON') {
        this.nightMode = true;
      }
    }
  }
  ngOnInit() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (!resp.language_code) {
          return
        }
        this.lankeyname = resp.language_code;
        this.lankey = resp.language_code;
      },
        err => {
          console.log(err);
        });

    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") == 'live') {
        this.isBooleanforLive = true;
        this.isBooleanforDash = false;
      } else {
        if (localStorage.getItem("SCREEN") == 'dashboard') {
          this.isBooleanforDash = true;
          this.isBooleanforLive = false;
        }
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  setLanguage(key) {
    var payload = {
      uid: this.islogin._id,
      lang: key
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
      .subscribe((resp) => {
        console.log('response language code ' + resp);
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 1500,
          position: 'bottom'
        });
        toast.present();
      });
    this.events.publish('lang:key', key);
  }

  setNotif(notif) {
    this.events.publish('notif:updated', notif);
    this.isChecked = notif;
    if (notif === true) {
      this.tts.speak('You have succesfully enabled voice notifications')
        .then(() => console.log('Success'))
        .catch((reason: any) => console.log(reason));
    }
  }

  rootpage() {
    let alert = this.alertCtrl.create();
    alert.setSubTitle('Choose default screen');
    alert.addInput({
      type: 'radio',
      label: 'Dashboard',
      value: 'dashboard',
      checked: this.isBooleanforDash
    });

    alert.addInput({
      type: 'radio',
      label: 'Live Tracking',
      value: 'live',
      checked: this.isBooleanforLive
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log(data)
        localStorage.setItem("SCREEN", data);
        const toast = this.toastCtrl.create({
          message: 'Default page set to ' + data + ' page',
          duration: 2000
        });
        toast.present();
      }
    });
    alert.present();
  }

  onChangeMap(key) {
    console.log("map key changed: ", key)
    localStorage.setItem("MAP_KEY", key);
  }

  onChangeFuel(key) {
    console.log("key changed: ", key)
    const newContact = {
      fname: this.islogin.fn,
      lname: this.islogin.ln,
      org: this.islogin._orgName,
      uid: this.islogin._id,
      fuel_unit: key
    }

    var _baseURl = this.apiCall.mainUrl + "users/Account_Edit";
    this.apiCall.urlpasseswithdata(_baseURl, newContact)
      .subscribe(data => {
        console.log("got response data: ", data)
        var logindetails = JSON.parse(JSON.stringify(data));
        var userDetails = window.atob(logindetails.token.split('.')[1]);
        var details = JSON.parse(userDetails);
        localStorage.setItem('details', JSON.stringify(details));
      })
  }

  showAlert() {
    const prompt = this.alertCtrl.create({
      title: 'Immobilize Password',
      message: "Enter password for engine cut",
      inputs: [
        {
          name: 'password',
          placeholder: 'Password'
        },
        {
          name: 'cpassword',
          placeholder: 'Confirm Password'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            console.log("data: ", data)
            if (data.password !== data.cpassword) {
              this.toastmsg("Entered password and confirm password did not match.")
              return;
            }
            this.setPassword(data);
          }
        }
      ]
    });
    prompt.present();
  }

  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }

  setPassword(data) {
    var payload = {
      uid: this.islogin._id,
      engine_cut_psd: data.password
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
      .subscribe((resp) => {
        console.log('response language code ' + resp);
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 1500,
          position: 'bottom'
        });
        toast.present();
      });
  }

  update(ev) {
    console.log("invoking notification: ", ev);
    if (ev === true) {
      localStorage.setItem('NightMode', 'ON');
    } else {
      localStorage.setItem('NightMode', 'OFF');
    }
  }

  notifSet() {
    this.navCtrl.push('NotifSettingPage');
  }

}
