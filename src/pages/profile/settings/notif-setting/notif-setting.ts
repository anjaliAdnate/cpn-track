import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../../providers/api-service/api-service';
import { NotifModalPage } from './notif-modal';
// Add this import after the rest
import { NativeAudio } from '@ionic-native/native-audio';

@IonicPage()
@Component({
  selector: 'page-notif-setting',
  templateUrl: 'notif-setting.html',
})
export class NotifSettingPage {
  // notifArray: any[] = [];
  islogin: any;
  notifArray: any = {
    ign: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    poi: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    power: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    fuel: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    geo: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    overspeed: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    AC: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    route: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    maxstop: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    sos: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    },
    sms: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails: [], phones: []
    }
  };
  dataOriginal: any;
  fData: any = {};
  notifType: any;
  isAddEmail: boolean = false;
  emailList: any;
  isAddPhone: boolean = false;
  phonelist: any;
  shownotifdiv: boolean = true;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private modalCtrl: ModalController,
    public platform: Platform,
    private nativeAudio: NativeAudio
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    console.log("check here: ", this.notifArray);
    this.getDetail();

    // The Native Audio plugin can only be called once the platform is ready
    this.platform.ready().then(() => {
      console.log("platform ready");

      // This is used to unload the track. It's useful if you're experimenting with track locations
      this.nativeAudio.unload('trackID').then(function () {
        console.log("unloaded audio!");
      }, function (err) {
        console.log("couldn't unload audio... " + err);
      });

      // 'trackID' can be anything
      this.nativeAudio.preloadComplex('trackID', 'assets/audio/test.mp3', 1, 1, 0).then(function () {
        console.log("audio loaded!");
      }, function (err) {
        console.log("audio failed: " + err);
      });
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotifSettingPage');
  }

  onChange(ev) {
    console.log("event: ", ev)
  }

  getDetail() {
    var _bUrl = this.apiCall.mainUrl + 'users/getCustumerDetail?uid=' + this.islogin._id;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        // debugger
        console.log("resp data: ", respData)
        if (respData.length === 0) {
          return;
        } else {
          this.notifArray = respData['cust'].alert;
          console.log("kkkk", this.notifArray);
          this.dataOriginal = JSON.parse(JSON.stringify(respData['cust'].alert));
          // this.notifArray = JSON.parse(JSON.stringify(respData.cust.alert));

          console.log("notif array list : " + this.notifArray.ign.sms_status);
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  tab(key1, key2) {
    if (key1 && key2) {
      if (this.notifArray[key1][key2] === 'false') {
        this.notifArray[key1][key2] = true;
      } else {
        this.notifArray[key1][key2] = false;
      }
    }
    // this.notifArray[key1][key2] = !!!this.notifArray[key1][key2];

    if (JSON.stringify(this.notifArray) === JSON.stringify(this.dataOriginal))
      return;

    this.fData.contactid = this.islogin._id;
    this.fData.alert = this.notifArray;
    var url = this.apiCall.mainUrl + 'users/editUserDetails';
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, this.fData)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log("check stat: ", respData);
        if (respData) {
          this.toastCtrl.create({
            message: 'Setting Updated',
            duration: 1500,
            position: 'bottom'
          }).present();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  tab1(key1, key2, value) {

    console.log("playing audio");

    // this.nativeAudio.play('trackID').then(function () {
    //   console.log("playing audio!");
    // }, function (err) {
    //   console.log("error playing audio: " + err);
    // });

    if (key1 && key2)
      if (!value) {
        if (this.notifArray[key1][key2] === 'false') {
          this.notifArray[key1][key2] = true;
        } else {
          this.notifArray[key1][key2] = false;
        }
      }
      // this.notifArray[key1][key2] = !!!this.notifArray[key1][key2];
      else {
        this.notifArray[key1][key2] = value;
      }
    // this.notifArray[key1][key2] = value;
    if (JSON.stringify(this.notifArray) === JSON.stringify(this.dataOriginal))
      return;

    this.fData.contactid = this.islogin._id;
    this.fData.alert = this.notifArray;
    var url = this.apiCall.mainUrl + 'users/editUserDetails';
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, this.fData)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log("check stat: ", respData);
        if (respData) {
          this.toastCtrl.create({
            message: 'Setting Updated',
            duration: 1500,
            position: 'bottom'
          }).present();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  addEmail(noti) {
    // use AudioProvider to control selected track 

    this.notifType = noti;
    this.isAddEmail = true;
    var data = {
      "buttonClick": 'email',
      "notifType": this.notifType,
      "compData": this.notifArray
    }

    const modal = this.modalCtrl.create(NotifModalPage, {
      "notifData": data
    });
    modal.present();
  }

  addPhone(noti) {
    this.notifType = noti;
    this.isAddPhone = true;
    var data = {
      buttonClick: 'phone',
      "notifType": this.notifType,
      "compData": this.notifArray
    }

    const modal = this.modalCtrl.create(NotifModalPage, {
      "notifData": data
    });
    modal.present();
  }

}
