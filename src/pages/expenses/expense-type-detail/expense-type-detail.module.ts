import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpenseTypeDetailPage } from './expense-type-detail';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ExpenseTypeDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ExpenseTypeDetailPage),
    TranslateModule.forChild()
  ],
})
export class ExpenseTypeDetailPageModule {}
