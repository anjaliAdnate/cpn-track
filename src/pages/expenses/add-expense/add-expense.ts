import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-add-expense',
  templateUrl: 'add-expense.html',
})
export class AddExpensePage implements OnInit {
  portstemp: any[] = [];
  islogin: any;
  veh_id: any = {};
  vehName: any;
  reference: any;
  payModeValue: any;
  currencyArray: any[] = [];
  currencyJson: any[] = [];
  currencyValue: any = 'INR';

  currency: any;
  amt: number;
  expenseTypeValue: any;
  ddate: any;

  expenseType = [
    { "value": "fuel", "viewValue": "Fuel" },
    { "value": "labor", "viewValue": "Labor" },
    { "value": "service", "viewValue": "Service" },
    { "value": "tools", "viewValue": "Tools" },
    { "value": "toll", "viewValue": "Toll" },
    { "value": "salary", "viewValue": "Salary" },
    { "value": "other", "viewValue": "Others" }];
  payMode = [
    { "value": "CASH", "viewValue": "Cash" },
    { "value": "CHEQUE", "viewValue": "Cheque" },
    { "value": "ONLINE", "viewValue": "Online" },
    { "value": "OTHER", "viewValue": "Other" }
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private translate: TranslateService
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    if (!this.navParams.get('vehicleList')) {
      this.navCtrl.pop();
      return;
    }
    this.portstemp = this.navParams.get("vehicleList");
    this.apiCall.getCurrency().subscribe((data) => {
      this.currencyJson = data;
      this.getCurrencies();
    })
  }
  ngOnInit() {

  }
  getCurrencies() {
    this.currency = [];
    var currencyObj = this.currencyJson;
    // console.log('currencyObj', currencyObj);

    for (var i in currencyObj) {

      if (this.veh_id.currency != undefined) {
        var devicecurr = this.veh_id.device.currency;
        if (i == devicecurr) {
          // this.currencyValue = i + '-' + currencyObj[i];
          this.currencyValue = i;
        }

      } else {
        if (i == 'INR') {
          // this.currencyValue = i + '-' + currencyObj[i];
          this.currencyValue = i;
        }
      }

      var tempObj = {
        currencyVal: i,
        country: i + '-' + currencyObj[i]
      }
      this.currencyArray.push(tempObj);
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddExpensePage');
  }

  onChnageEvent(veh) {
    let that = this;
    that.veh_id = veh;
    this.getCurrencies();
  }

  submit() {
    if (this.amt == undefined || this.currencyValue == undefined || this.expenseTypeValue == undefined || this.ddate == undefined || this.vehName == undefined || this.currency == undefined) {
      this.toastMessage(this.translate.instant('Please fill all the fields and try again'));
      return;
    }
    var _burl = this.apiCall.mainUrl + 'expense/addExpense';
    var payload = {
      "user": this.islogin._id,
      "currency": this.currencyValue,
      "vehicle": this.veh_id._id,
      "payment_mode": this.payModeValue,
      "amount": Number(this.amt),
      "odometer": this.veh_id.total_odo,
      "type": this.expenseTypeValue,
      "date": new Date(this.ddate).toISOString()
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(_burl, payload)
      .subscribe(data => {
        this.apiCall.stopLoading();
        if (data.message === "added Successfully") {
          this.toastMessage(this.translate.instant('dealeradded', { value: 'Expense' }));
          this.navCtrl.pop();
        } else {
          this.toastMessage(this.translate.instant('Process failed, something went wrong'));
        }
      });
  }

  toastMessage(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

}
