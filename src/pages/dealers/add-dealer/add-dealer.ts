import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-add-customer-model',
  templateUrl: 'add-dealer.html',
})
export class AddDealerPage {
  addDealerform: FormGroup;
  islogin: any;
  submitAttempt: boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public apicall: ApiServiceProvider,
    public alerCtrl: AlertController,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    private translate: TranslateService
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));

    this.addDealerform = formBuilder.group({
      userId: [this.islogin.phn, Validators.required],
      Firstname: ['', Validators.required],
      LastName: ['', Validators.required],
      emailid: ['', Validators.email],
      contact_num: [''],
      password: ['', Validators.required],
      cpassword: ['', Validators.required],
      address: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddDealerPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  _submit() {
    this.submitAttempt = true;
    if (this.addDealerform.valid) {
      if (this.addDealerform.value.password != this.addDealerform.value.cpassword) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Password mismatched!'),
          duration: 1500,
          position: 'middle'
        })
        toast.present();
      } else {
        if (this.addDealerform.value.contact_num == "") {
          let toast = this.toastCtrl.create({
            message: this.translate.instant('Enter mobile number'),
            duration: 1500,
            position: 'middle'
          })
          toast.present();
        } else {
          var payload = {
            address: this.addDealerform.value.address,
            custumer: false,
            email: this.addDealerform.value.emailid,
            first_name: this.addDealerform.value.Firstname,
            isDealer: true,
            last_name: this.addDealerform.value.LastName,
            org_name: this.islogin._orgName,
            password: this.addDealerform.value.password,
            phone: this.addDealerform.value.contact_num,
            supAdmin: this.islogin._id,
            sysadmin: this.islogin.isSuperAdmin,
            user_id: this.addDealerform.value.userId
          }

          this.apicall.startLoading().present();
          this.apicall.signupApi(payload)
            .subscribe(data => {
              this.apicall.stopLoading();
              let toast = this.toastCtrl.create({
                message: this.translate.instant('dealeradded', {value: this.translate.instant('Dealers')}),
                position: 'top',
                duration: 1500
              });

              toast.onDidDismiss(() => {
                this.viewCtrl.dismiss();
              });

              toast.present();
            },
              err => {
                this.apicall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var namepass = [];
                namepass = msg.split(":");
                var name = namepass[1];
                let alert = this.alerCtrl.create({
                  message: name,
                  buttons: [this.translate.instant('Okay')]
                });
                alert.present();
              });

        }
      }
    }
  }

}
