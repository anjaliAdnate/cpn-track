import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelChartPage } from './fuel-chart';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FuelChartPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelChartPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class FuelChartPageModule {}
