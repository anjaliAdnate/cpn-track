import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import * as moment from 'moment';

import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-maintenance-reminder',
  templateUrl: 'maintenance-reminder.html',
})
export class MaintenanceReminderPage implements OnInit {
  islogin: any;
  portstemp: any[] = [];
  selectedVehicle: any;
  reminderType: string;
  reminderTypes: any = [{
    viewValue: "Service",
    value: "Service",
  }, {
    viewValue: "Oil Change",
    value: "oil_change",
  }, {
    viewValue: "Tyres",
    value: "Tyres",
  }, {
    viewValue: "Maintenance",
    value: "Maintenance",
  },
  {
    viewValue: "Others",
    value: "Others",
  }
  ]
  datetimeStart: any;
  datetimeEnd: any;
  remData: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private modalCtrl: ModalController,
    private toastCtrl: ToastController
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    // this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeStart = moment().subtract(1, 'months').format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('end date', this.datetimeEnd);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaintenanceReminderPage');
  }

  ngOnInit() {
    this.getdevices();
    this.getReminders();
  }

  onSeletChange(rem) {
    console.log("reminder changed: ", this.reminderType)
    this.remData = [];
    this.getReminders();
  }

  onChangeVehicle() {
    console.log("vehicle changed: ", this.selectedVehicle)
    this.remData = [];
    this.getReminders();
  }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }

    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {

        this.portstemp = data.devices;
      },
        err => {

          console.log(err);
        });
  }

  getReminders() {
    const Burl = this.apiCall.mainUrl + 'reminder/reminderdatatable';
    var payload = {};
    if (this.islogin.isSuperAdmin === true) {
      if (this.reminderType != undefined && this.selectedVehicle != undefined) {
        payload = {
          "draw": 2,
          "columns": [
            {
              "data": "_id"
            },
            {
              "data": "user.first_name"
            },
            {
              "data": "user.last_name"
            },
            {
              "data": "device.Device_Name"
            },
            {
              "data": "reminder_date"
            },
            {
              "data": "notification_type.SMS"
            },
            {
              "data": "notification_type.EMAIL"
            },
            {
              "data": "notification_type.PUSH_NOTIFICATION"
            },
            {
              "data": "reminder_type"
            },
            {
              "data": "prior_reminder"
            },
            {
              "data": "note"
            }
          ],
          "order": [
            {
              "column": 0,
              "dir": "asc"
            }
          ],
          "start": 0,
          "length": 25,
          "search": {
            "value": "",
            "regex": false
          },
          "op": {},
          "select": [],
          "find": {
            "$or": [
              {
                "user": this.islogin._id
              },
              {}
            ],
            "device": {
              "$in": [
                this.selectedVehicle._id
              ]
            },
            "reminder_date": {
              "$lte": new Date(this.datetimeEnd).toISOString()
            },
            "reminder_type": this.reminderType
          }
        };
      } else {
        if (this.reminderType != undefined && this.selectedVehicle == undefined) {
          payload = {
            "draw": 2,
            "columns": [
              {
                "data": "_id"
              },
              {
                "data": "user.first_name"
              },
              {
                "data": "user.last_name"
              },
              {
                "data": "device.Device_Name"
              },
              {
                "data": "reminder_date"
              },
              {
                "data": "notification_type.SMS"
              },
              {
                "data": "notification_type.EMAIL"
              },
              {
                "data": "notification_type.PUSH_NOTIFICATION"
              },
              {
                "data": "reminder_type"
              },
              {
                "data": "prior_reminder"
              },
              {
                "data": "note"
              }
            ],
            "order": [
              {
                "column": 0,
                "dir": "asc"
              }
            ],
            "start": 0,
            "length": 25,
            "search": {
              "value": "",
              "regex": false
            },
            "op": {},
            "select": [],
            "find": {
              "$or": [
                {
                  "user": this.islogin._id
                },
                {}
              ],
              "reminder_date": {
                "$lte": new Date(this.datetimeEnd).toISOString()
              },
              "reminder_type": this.reminderType
            }
          };
        } else {
          if (this.reminderType == undefined && this.selectedVehicle != undefined) {
            payload = {
              "draw": 2,
              "columns": [
                {
                  "data": "_id"
                },
                {
                  "data": "user.first_name"
                },
                {
                  "data": "user.last_name"
                },
                {
                  "data": "device.Device_Name"
                },
                {
                  "data": "reminder_date"
                },
                {
                  "data": "notification_type.SMS"
                },
                {
                  "data": "notification_type.EMAIL"
                },
                {
                  "data": "notification_type.PUSH_NOTIFICATION"
                },
                {
                  "data": "reminder_type"
                },
                {
                  "data": "prior_reminder"
                },
                {
                  "data": "note"
                }
              ],
              "order": [
                {
                  "column": 0,
                  "dir": "asc"
                }
              ],
              "start": 0,
              "length": 25,
              "search": {
                "value": "",
                "regex": false
              },
              "op": {},
              "select": [],
              "find": {
                "$or": [
                  {
                    "user": this.islogin._id
                  },
                  {}
                ],
                "device": {
                  "$in": [
                    this.selectedVehicle._id
                  ]
                },
                "reminder_date": {
                  "$lte": new Date(this.datetimeEnd).toISOString()
                }
              }
            };
          } else {
            if (this.reminderType == undefined && this.selectedVehicle == undefined) {
              payload = {
                "draw": 2,
                "columns": [
                  {
                    "data": "_id"
                  },
                  {
                    "data": "user.first_name"
                  },
                  {
                    "data": "user.last_name"
                  },
                  {
                    "data": "device.Device_Name"
                  },
                  {
                    "data": "reminder_date"
                  },
                  {
                    "data": "notification_type.SMS"
                  },
                  {
                    "data": "notification_type.EMAIL"
                  },
                  {
                    "data": "notification_type.PUSH_NOTIFICATION"
                  },
                  {
                    "data": "reminder_type"
                  },
                  {
                    "data": "prior_reminder"
                  },
                  {
                    "data": "note"
                  }
                ],
                "order": [
                  {
                    "column": 0,
                    "dir": "asc"
                  }
                ],
                "start": 0,
                "length": 25,
                "search": {
                  "value": "",
                  "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                  "$or": [
                    {
                      "user": this.islogin._id
                    },
                    {}
                  ],
                  "reminder_date": {
                    "$lte": new Date(this.datetimeEnd).toISOString()
                  }
                }
              };
            }
          }
        }
      }
    } else {
      if (this.reminderType != undefined && this.selectedVehicle != undefined) {
        payload = {
          "draw": 2,
          "columns": [
            {
              "data": "_id"
            },
            {
              "data": "user.first_name"
            },
            {
              "data": "user.last_name"
            },
            {
              "data": "device.Device_Name"
            },
            {
              "data": "reminder_date"
            },
            {
              "data": "notification_type.SMS"
            },
            {
              "data": "notification_type.EMAIL"
            },
            {
              "data": "notification_type.PUSH_NOTIFICATION"
            },
            {
              "data": "reminder_type"
            },
            {
              "data": "prior_reminder"
            },
            {
              "data": "note"
            }
          ],
          "order": [
            {
              "column": 0,
              "dir": "asc"
            }
          ],
          "start": 0,
          "length": 25,
          "search": {
            "value": "",
            "regex": false
          },
          "op": {},
          "select": [],
          "find": {
            "$or": [
              {
                "user": this.islogin._id
              },
              {
                "created_by": this.islogin.supAdmin
              }
            ],
            "device": {
              "$in": [
                this.selectedVehicle._id
              ]
            },
            "reminder_date": {
              "$lte": new Date(this.datetimeEnd).toISOString()
            },
            "reminder_type": this.reminderType
          }
        };
      } else {
        if (this.reminderType != undefined && this.selectedVehicle == undefined) {
          payload = {
            "draw": 2,
            "columns": [
              {
                "data": "_id"
              },
              {
                "data": "user.first_name"
              },
              {
                "data": "user.last_name"
              },
              {
                "data": "device.Device_Name"
              },
              {
                "data": "reminder_date"
              },
              {
                "data": "notification_type.SMS"
              },
              {
                "data": "notification_type.EMAIL"
              },
              {
                "data": "notification_type.PUSH_NOTIFICATION"
              },
              {
                "data": "reminder_type"
              },
              {
                "data": "prior_reminder"
              },
              {
                "data": "note"
              }
            ],
            "order": [
              {
                "column": 0,
                "dir": "asc"
              }
            ],
            "start": 0,
            "length": 25,
            "search": {
              "value": "",
              "regex": false
            },
            "op": {},
            "select": [],
            "find": {
              "$or": [
                {
                  "user": this.islogin._id
                },
                {
                  "created_by": this.islogin.supAdmin
                }
              ],
              "reminder_date": {
                "$lte": new Date(this.datetimeEnd).toISOString()
              },
              "reminder_type": this.reminderType
            }
          };
        } else {
          if (this.reminderType == undefined && this.selectedVehicle != undefined) {
            payload = {
              "draw": 2,
              "columns": [
                {
                  "data": "_id"
                },
                {
                  "data": "user.first_name"
                },
                {
                  "data": "user.last_name"
                },
                {
                  "data": "device.Device_Name"
                },
                {
                  "data": "reminder_date"
                },
                {
                  "data": "notification_type.SMS"
                },
                {
                  "data": "notification_type.EMAIL"
                },
                {
                  "data": "notification_type.PUSH_NOTIFICATION"
                },
                {
                  "data": "reminder_type"
                },
                {
                  "data": "prior_reminder"
                },
                {
                  "data": "note"
                }
              ],
              "order": [
                {
                  "column": 0,
                  "dir": "asc"
                }
              ],
              "start": 0,
              "length": 25,
              "search": {
                "value": "",
                "regex": false
              },
              "op": {},
              "select": [],
              "find": {
                "$or": [
                  {
                    "user": this.islogin._id
                  },
                  {
                    "created_by": this.islogin.supAdmin
                  }
                ],
                "device": {
                  "$in": [
                    this.selectedVehicle._id
                  ]
                },
                "reminder_date": {
                  "$lte": new Date(this.datetimeEnd).toISOString()
                }
              }
            };
          } else {
            if (this.reminderType == undefined && this.selectedVehicle == undefined) {
              payload = {
                "draw": 2,
                "columns": [
                  {
                    "data": "_id"
                  },
                  {
                    "data": "user.first_name"
                  },
                  {
                    "data": "user.last_name"
                  },
                  {
                    "data": "device.Device_Name"
                  },
                  {
                    "data": "reminder_date"
                  },
                  {
                    "data": "notification_type.SMS"
                  },
                  {
                    "data": "notification_type.EMAIL"
                  },
                  {
                    "data": "notification_type.PUSH_NOTIFICATION"
                  },
                  {
                    "data": "reminder_type"
                  },
                  {
                    "data": "prior_reminder"
                  },
                  {
                    "data": "note"
                  }
                ],
                "order": [
                  {
                    "column": 0,
                    "dir": "asc"
                  }
                ],
                "start": 0,
                "length": 25,
                "search": {
                  "value": "",
                  "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                  "$or": [
                    {
                      "user": this.islogin._id
                    },
                    {
                      "created_by": this.islogin.supAdmin
                    }
                  ],
                  "reminder_date": {
                    "$lte": new Date(this.datetimeEnd).toISOString()
                  }
                }
              };
            }
          }
        }
      }
    }

    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(Burl, payload)
      .subscribe(resData => {
        this.apiCall.stopLoading();
        console.log("response reminder: " + resData);
        if (resData.data.length > 0) {
          for (var i = 0; i < resData.data.length; i++) {
            debugger
            var date1 = moment(new Date(resData.data[i].reminder_date), 'DD/MM/YYYY').format("llll");
            var str = date1.split(', ');
            var day = str[0];
            var date3 = str[1].split(' ');
            var date4 = str[2].split(' ');
            var time = date4[1] + ' ' + date4[2];
            var year = date4[0];
            var month = date3[0];
            var dateNum = date3[1];
            this.remData.push({
              datFormats: {
                'day': day,
                'time': time,
                'year': year,
                'month': month,
                'dateNum': dateNum
              },
              vehiclName: resData.data[i].device.Device_Name,
              reminderType: resData.data[i].reminder_type,
              notifType: resData.data[i].notification_type,
              note: resData.data[i].note,
              prior_reminder: resData.data[i].prior_reminder
            })
          }
        } else {
          this.toastCtrl.create({
            message: 'Reminders not found...',
            duration: 1500,
            position: 'bottom'
          }).present();
        }
        // if (resData.data.length > 0) {
        //   this.remData = resData.data;
        // } else {
        //   this.toastCtrl.create({
        //     message: 'Reminders not found...',
        //     duration: 1500,
        //     position: 'bottom'
        //   }).present();
        // }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  onAddReminder() {
    const modal = this.modalCtrl.create('AddReminderPage');
    modal.present();
    modal.onDidDismiss(() => {
      this.remData = [];
      this.getReminders();
    })
  }

}
