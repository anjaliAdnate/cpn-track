import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-geofence-report',
  templateUrl: 'geofence-report.html',
})
export class GeofenceReportPage implements OnInit {

  geofenceRepoert: any[] = [];
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  geofencelist: any;
  devicesReport: any;
  geofencedata: any[];
  StartTime: string;
  Startetime: string;
  Startdate: string;
  datetime: number;
  geofenceReportdata: any[] = [];
  locationEndAddress: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallGeofenceReport: ApiServiceProvider,
    public toastCtrl: ToastController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getgeofence1();
  }

  getgeofence1() {
    this.apicallGeofenceReport.startLoading().present();
    this.apicallGeofenceReport.getallgeofenceCall(this.islogin._id)
      .subscribe(data => {
        this.apicallGeofenceReport.stopLoading();
        this.geofencelist = data;
        console.log("geofencelist=> ", this.geofencelist)
      },
        err => {
          this.apicallGeofenceReport.stopLoading();
          console.log(err)
        });
  }

  getGeofencedata(geofence) {
    console.log("selectedVehicle=> ", geofence)
    this.Ignitiondevice_id = geofence._id;
    this.getGeofenceReport();
  }

  getGeofenceReport() {
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    let that = this;
    this.apicallGeofenceReport.startLoading().present();
    this.apicallGeofenceReport.getGeogenceReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicallGeofenceReport.stopLoading();
        this.geofenceRepoert = data;
        if (this.geofenceRepoert.length > 0) {
          this.innerFunc(this.geofenceRepoert)
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not fond for selected Dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallGeofenceReport.stopLoading();
        console.log(error);
      });
  }

  innerFunc(geofenceRepoert) {
    let outerthis = this;
    outerthis.geofenceReportdata = [];
    var i = 0, howManyTimes = geofenceRepoert.length;
    function f() {
      outerthis.locationEndAddress = undefined;
      outerthis.geofenceReportdata.push({
        'device': outerthis.geofenceRepoert[i].device,
        'vehicleName': outerthis.geofenceRepoert[i].vehicleName,
        'direction': outerthis.geofenceRepoert[i].direction,
        'timestamp': outerthis.geofenceRepoert[i].timestamp,
        '_id': outerthis.geofenceRepoert[i]._id,
        'start_location': {
          'lat': outerthis.geofenceRepoert[i].lat,
          'long': outerthis.geofenceRepoert[i].long
        }
      });

      outerthis.start_address(outerthis.geofenceReportdata[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  start_address(item, index) {
    let that = this;
    that.geofenceReportdata[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.geofenceReportdata[index].StartLocation = "N/A";
    } else if (item.start_location) {
      var payload = {
        "lat": item.start_location.lat,
        "long": item.start_location.long,
        "api_id": "2"
      }
      this.apicallGeofenceReport.getAddressApi(payload)
        .subscribe((data) => {
          console.log("got address: " + data.results)
          if (data.results[2] != undefined || data.results[2] != null) {
            that.geofenceReportdata[index].StartLocation = data.results[2].formatted_address;
          } else {
            that.geofenceReportdata[index].StartLocation = 'N/A';
          }

        })
    }
  }

}
