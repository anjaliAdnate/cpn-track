import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentGreetingPage } from './payment-greeting';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PaymentGreetingPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentGreetingPage),
    TranslateModule.forChild()
  ],
})
export class PaymentGreetingPageModule {}
