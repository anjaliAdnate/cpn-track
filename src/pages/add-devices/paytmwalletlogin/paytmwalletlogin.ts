import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
declare var RazorpayCheckout: any;

@IonicPage()
@Component({
  selector: 'page-paytmwalletlogin',
  templateUrl: 'paytmwalletlogin.html',
})
export class PaytmwalletloginPage {
  paytmmail: any;
  paytmnumber: any;
  paytmotp: number;
  successresponse: boolean = false;
  inputform: boolean = false;
  razor_key: string = 'rzp_live_jB4onRx1BUUvxt';
  paymentAmount: number = 120000;
  currency: any = 'INR';
  parameters: any;
  islogin: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider,
    private menu: MenuController,
    public toast: ToastController
  ) {
    // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
    this.paytmnumber = this.islogin.phn;
    this.parameters = navParams.get('param');
    console.log("parameters", this.parameters)
  }
  ionViewDidEnter() {

    this.menu.enable(true);
  }

  paytmSignupOTPResponse: any;

  payWithRazor() {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/GO0jiDP.jpg',
      currency: this.currency, // your 3 letter currency code
      key: this.razor_key, // your Key Id from Razorpay dashboard
      amount: this.paymentAmount, // Payment amount in smallest denomiation e.g. cents for USD
      name: this.parameters.Device_Name,
      prefill: {
        email: this.islogin.email,
        contact: this.paytmnumber,
        name: this.islogin.fn + ' ' + this.islogin.ln
      },
      theme: {
        color: '#d80622'
      },
      modal: {
        ondismiss: function () {
          console.log('dismissed')
        }
      }
    };

    var successCallback = function (payment_id) {
      alert('payment_id: ' + payment_id);
    };

    var cancelCallback = function (error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }

  paytmwallet() {

    var useDetails = localStorage.getItem('details');
    var userId = JSON.parse(useDetails)._id;

    // payment gateway integration function
    var paytmUserCredentials = {
      "user": userId,
      "app_id": "OneQlikVTS",
      "phone": this.paytmnumber,
      "scope": "wallet",
      "responseType": "token",
    }

    this.apiCall.paytmLoginWallet(paytmUserCredentials)
      .subscribe(res => {
        this.successresponse = true;
        this.inputform = true;
        this.paytmSignupOTPResponse = res;
        let errToast_success = this.toast.create({
          message: 'OTP sent successfully, Please type OTP in next field !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        errToast_success.present();

      }, err => {
        let errToast = this.toast.create({
          message: 'Server error , Please try after somtime !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        if (err) {
          console.log("error occured !!!");
          errToast.present();
        }
      })
  }

  goBack() {
    this.navCtrl.pop();
  }
  resndOTP() {
    var useDetails = localStorage.getItem('details');
    var userId = JSON.parse(useDetails)._id;
    var paytmUserCredentials = {
      "user": userId,
      "app_id": "OneQlikVTS",
      "phone": this.paytmnumber,
      "scope": "wallet",
      "responseType": "token"
    }
    this.apiCall.paytmLoginWallet(paytmUserCredentials)
      .subscribe(res => {
        this.successresponse = true;
        this.inputform = true;
        this.paytmSignupOTPResponse = res;
        let errToast_success = this.toast.create({
          message: 'OTP sent successfully, Please type OTP in next field !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        errToast_success.present();

      }, err => {
        let errToast = this.toast.create({
          message: 'Server error , Please try after somtime !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        if (err) {
          errToast.present();
        }
      })
    //  payment gateway integration function
  }

  navOptions = {
    animation: 'ios-transition'
  };
  paytmAuthantication() {
    this.apiCall.startLoading();
    var otpCredentials = {
      "otp": this.paytmotp,
      "transac_id": this.paytmSignupOTPResponse.transac_id,
      "app_id": "OneQlikVTS"
    }
    this.apiCall.paytmOTPvalidation(otpCredentials)
      .subscribe(res => {
        console.log("response from wallet page=> ", res)
        this.apiCall.stopLoading();
        // debugger;
        var s = JSON.parse(res.response)
        if (s.status == 'FAILURE') {
          let toast = this.toastCtrl.create({
            message: s.message,
            duration: 2000,
            position: "bottom"
          })

          toast.onDidDismiss(() => {
            this.paytmotp = undefined;
          });

          toast.present();
        } else {
          localStorage.setItem('paytmregNum', this.paytmnumber);
          this.navCtrl.push("WalletPage", null, this.navOptions);
        }
      }, err => {
        console.log("error found=> " + err)
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: 'internal server Error, Please try after sometime !!!',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      })
  }
}
